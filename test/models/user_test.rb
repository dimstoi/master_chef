require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user=User.new(name:"test user", email:"test@yahoo.com", password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do 
    assert @user.valid?
  end

  test "name should be present" do
    @user.name="  "
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a" * 256 + "@yahoo.com"
    assert_not @user.valid?
  end

  test "email validation should reject invalid addresses" do
    @user.email = "test@yahoo"
    assert_not @user.valid?
    @user.email = "test.yahoo.com"
    assert_not @user.valid?      
  end

  test "email addresses should be unique" do
    second_user = User.new(name:"second user", email:"test@yahoo.com", password: "foobar", password_confirmation: "foobar")    
    @user.save
    assert_not second_user.valid?
  end

  test "password should be not empty and not just blanks" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
    @user.password = @user.password_confirmation = ""
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "abc"
    assert_not @user.valid?
  end

end
