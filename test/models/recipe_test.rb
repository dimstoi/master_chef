require 'test_helper'

class RecipeTest < ActiveSupport::TestCase

  def setup    
    @recipe=recipes(:test_recipe1)
  end

  test "should be valid" do
    assert @recipe.valid?
  end

  test "user id should be present" do
    @recipe.user_id = nil
    assert_not @recipe.valid?
  end

  test "title should be present" do
    @recipe.title = nil
    assert_not @recipe.valid?
  end

  test "description should be present" do
    @recipe.description = nil
    assert_not @recipe.valid?
  end

  test "ingredients should be present" do
    @recipe.ingredients = nil
    assert_not @recipe.valid?
  end

  test "duration should be present" do
    @recipe.duration = nil
    assert_not @recipe.valid?
  end

  test "title should not exceed the max length" do
    @recipe.title = "a" * 61
    assert_not @recipe.valid?
  end

  test "description should not exceed the max length" do
    @recipe.description = "d"*2001
    assert_not @recipe.valid?
  end

  test "ingredients should not exceed the max length" do
    @recipe.ingredients = "d"*801
    assert_not @recipe.valid?
  end

end
