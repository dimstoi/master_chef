require 'test_helper'

class CommentTest < ActiveSupport::TestCase

  def setup    
    @comment=comments(:comment1)
  end

  test "should be valid" do
    assert @comment.valid?, @comment.errors.full_messages.inspect
  end

  test "user id should be present" do
    @comment.user_id = nil
    assert_not @comment.valid?
  end

  test "recipe id should be present" do
    @comment.recipe_id = nil
    assert_not @comment.valid?
  end

  test "content should be present" do
    @comment.content = ""
    assert_not @comment.valid?
  end

  test "content should have a maximum length of 200" do
    @comment.content  = "a" * 300
    assert_not @comment.valid?
  end

end
