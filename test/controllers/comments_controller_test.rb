require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user=users(:dimitris)
    @recipe=recipes(:test_recipe1) 
  end

  test "create comment successfully" do    
    log_in_as(@user)                
    assert_difference '@recipe.comments.count', 1 do
      post recipe_comments_path(@recipe), params: { recipe_id: @recipe.id, comment: {content: "test"} }      
    end
  end

  test "create comment fails when not logged in" do    
    post recipe_comments_path(@recipe), params: {  recipe_id: @recipe.id , comment: { content: "test"} }      
    assert_redirected_to login_path
  end

end
