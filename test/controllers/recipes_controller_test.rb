require 'test_helper'

class RecipesControllerTest < ActionDispatch::IntegrationTest
  
  def setup    
    @recipe=recipes(:test_recipe1)
  end

  test "create recipe when not logged in fails" do
    post recipes_path, params: { recipe: { title: "title", description: "description", ingredients: "ingredients", duration: 45 } }
    assert_redirected_to login_url
  end

  test "show recipe when not logged in fails" do
    get recipe_path(@recipe)
    assert_redirected_to login_url
  end

  test "destroy recipe fails when not logged in" do
    assert_no_difference 'Recipe.count' do
      delete recipe_path(@recipe)
    end
    assert_redirected_to login_path 
  end

  test "destroy recipe fails when incorrect user" do
    log_in_as(users(:athina))
    assert_no_difference 'Recipe.count' do
      delete recipe_path(@recipe)
    end
    assert_redirected_to root_path 
  end

 

end
