require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "valid signup" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path params: { user: { name: "Test User",
                                      email: "test@yahoo.gr",
                                      password: "password",
                                      password_confirmation: "password" } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end

  test "invalid signup" do
    get signup_path # same as get new_user_path
    assert_no_difference 'User.count' do
      post users_path params: { user: { name: "",
                                      email: "test@yahoo",
                                      password: "password",
                                      password_confirmation: "password" } }
    end    
    assert_template 'users/new'
    assert_not is_logged_in?
  end


end
