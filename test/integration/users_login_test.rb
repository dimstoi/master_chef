require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    @user= users(:dimitris)
  end


  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path params:{ email: "", password: ""}
    assert_template 'sessions/new'    
  end

  test "login with valid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path, params:{ email: @user.email, password: 'foobar'}
    assert is_logged_in?
    assert_redirected_to @user # user_path(@user)
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", logout_path       
  end

  test "login and logout a user successfully" do
    get login_path
    post login_path, params:{ email: @user.email, password: 'foobar'}
    assert is_logged_in?
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
  end



end
