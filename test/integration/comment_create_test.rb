require 'test_helper'

class CommentCreateTest < ActionDispatch::IntegrationTest

  def setup
    @userA=users(:athina)
    @userB=users(:dimitris)
    @recipe=recipes(:test_recipe1)
  end

  test "create a comment successfully" do
    log_in_as(@userA)
    get recipe_path(@recipe)
    assert_template 'recipes/show'
    assert_difference '@recipe.comments.count', 1 do
      post recipe_comments_path(@recipe), params: { recipe_id: @recipe.id, comment: {  content: "test content" } }
    end
    assert_redirected_to recipe_path(@recipe)
    follow_redirect!
    assert_template 'recipes/show'
    assert_match "test content", response.body
  end

  test "create a comment unsuccessfully" do
    log_in_as(@userA)
    get recipe_path(@recipe)
    assert_template 'recipes/show'
    assert_no_difference '@recipe.comments.count' do
      post recipe_comments_path(@recipe), params: { recipe_id: @recipe.id, comment: {  content: "" } }
    end
    assert_redirected_to recipe_path(@recipe)
    follow_redirect!
    assert_template 'recipes/show'
    assert_no_match "test content", response.body
  end

end