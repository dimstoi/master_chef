require 'test_helper'

class RecipeCreateTest < ActionDispatch::IntegrationTest
  
  def setup    
    @user= users(:dimitris)
  end
  
  test "create a recipe successfully" do
    log_in_as(@user)
    get new_recipe_path   
    assert_template 'recipes/new'
    assert_difference 'Recipe.count', 1 do
      post recipes_path, params: { recipe: { title: "recipe_title", description: "description", ingredients: "ingredients", duration: 45 }}
    end  
    assert_redirected_to user_url(@user)
    follow_redirect!
    assert_template 'users/show'
    assert_match "recipe_title", response.body
  end


end
