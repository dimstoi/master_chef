require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user= users(:dimitris)
  end

  test "unsuccessful edit" do
    log_in_as(@user)  
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name: "", email: "foo", password: "",password_confirmation: "" } }
    assert_template 'users/edit'
  end

  test "successful edit" do
    log_in_as(@user)  
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name: "jimmy", email: "newmail@yahoo.gr", password: "",password_confirmation: "" } }
    assert_redirected_to @user
    @user.reload
    assert_equal @user.email, "newmail@yahoo.gr" 
    assert_equal @user.name, "jimmy"
  end

end
