class PagesController < ApplicationController
  def home
    query = params[:search_recipes].presence && params[:search_recipes][:query]  
    if query
      @recipes = Recipe.search_generic(query)      
    end
  end

  def contact
  end

  def about
  end
end
