class RecipesController < ApplicationController
  before_action :logged_in_user, only: [:create, :show, :destroy ]
  before_action :correct_user, only: [:destroy]
 
  def new
    @recipe=Recipe.new    
  end
    
  def create        
    @recipe=current_user.recipes.build(recipe_params)
    if @recipe.save
      flash[:success] = "Recipe created!"
      redirect_to user_path(current_user)
    else
      flash.now[:danger]="Recipe not saved!"
      render 'new'
    end
  end

  def show
    @recipe=Recipe.find_by(id: params[:id])        
    @comment=Comment.new 
    @comments=@recipe.comments.paginate(page: params[:page], :per_page => 10)   
  end  
  
  def destroy
    @recipe.destroy
    flash[:success] = "Recipe deleted!"
    redirect_to user_path(current_user)
  end


  private

  def recipe_params
    params.require(:recipe).permit(:title, :description, :ingredients, :image, :servings, :duration, :vegeterian)
  end

  def correct_user
    @recipe = current_user.recipes.find_by(id: params[:id])
    redirect_to root_url if @recipe.nil?
  end


end
