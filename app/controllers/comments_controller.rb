class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create ]
  
  def create          
    @recipe=Recipe.find(params[:recipe_id])  
    @comment=@recipe.comments.build(comment_params)
    @comment.user=current_user        
    @comment.save
    redirect_to recipe_path(@recipe)
  end

  private
  def comment_params
    params.require(:comment).permit(:content, :recipe_id, :user_id)
  end


end
