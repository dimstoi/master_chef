class Recipe < ApplicationRecord  
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  
  settings do
    mappings dynamic: false do
      indexes :title, type: :text, analyzer: :english
      indexes :ingredients, type: :text, analyzer: :english
      indexes :description, type: :text, analyzer: :english      
      indexes :vegeterian, type: :boolean
    end
  end

  belongs_to :user
  has_many :comments
  mount_uploader :image, RecipePictureUploader
  default_scope -> { order(created_at: :desc) }

  validates :user_id, presence: true
  validates :title, presence: true, length: {maximum: 60}
  validates :description, presence: true, length: {maximum: 2000}
  validates :ingredients, presence: true, length: { maximum: 800}
  validates :duration, presence: true
  validate  :image_size

  def image_size
    if image.size > 5.megabytes
      errors.add(:image, "should be less than 5MB")
    end
  end

  def self.search_generic(query)
    self.search({
      query: {
        bool: {
          must: [
          {
            multi_match: {
              query: query,
              fields: [ :title, :ingredients, :description]
            }
          }]
        }
      }
    })
  end

  def self.search_vegeterian(query)
    self.search({
      query: {
        bool: {
          must: [
          {
            multi_match: {
              query: query,
              fields: [ :title, :ingredients, :description]
            }
          },
          {
            match: {
              vegeterian: true
            }
          }]
        }
      }
    })
  end
  


end
