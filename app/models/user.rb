class User < ApplicationRecord
    mount_uploader :image, PictureUploader
    validates :name, presence: true, length: { maximum: 50}
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 255}, format: { with: VALID_EMAIL_REGEX }, uniqueness: true
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
    validate  :image_size
    has_many :comments, dependent:   :destroy
    has_many :recipes, dependent:   :destroy
   
    def User.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                      BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
    end

    private

    def image_size
        if image.size > 5.megabytes
          errors.add(:image, "should be less than 5MB")
        end
      end
  

end
