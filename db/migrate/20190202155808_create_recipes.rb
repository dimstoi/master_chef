class CreateRecipes < ActiveRecord::Migration[5.1]
  def change
    create_table :recipes do |t|
      t.string :title
      t.integer :servings
      t.text :description
      t.boolean :vegeterian
      t.text :ingredients
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
