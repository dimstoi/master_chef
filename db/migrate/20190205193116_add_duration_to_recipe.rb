class AddDurationToRecipe < ActiveRecord::Migration[5.1]
  def change
    add_column :recipes, :duration, :integer
  end
end
